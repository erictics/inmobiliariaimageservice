﻿

using AppWCFService.Contracts;
using System;
using System.Collections.Generic;
using System.Net;
using System.ServiceModel.Web;
using System.Web.Script.Services;
using AppWCFService.Controllers;
using Softv.Entities;
using System.Web;
using System.IO;

namespace AppWCFService
{
    [ScriptService]
    public partial class AppWCFService : ISistema
    {
        SistemaController SistemaController = new SistemaController();
        #region Almacen

        public List<logo> GetImagenesDepartamento()
        {
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS") return null;
            try
            {
                var request = HttpContext.Current.Request;
                int clv_departamento = Int32.Parse(request["id"].ToString());
                return SistemaController.GetImagenesDepartamento(clv_departamento);
            }
            catch (Exception ex)
            {
                throw new WebFaultException<string>(ex.Message, HttpStatusCode.InternalServerError);
            }
        }

        public List<logo> GetImagenesProyecto()
        {
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS") return null;
            try
            {
                var request = HttpContext.Current.Request;
                int clv_departamento = Int32.Parse(request["id"].ToString());
                return SistemaController.GetImagenesProyecto(clv_departamento);
            }
            catch (Exception ex)
            {
                throw new WebFaultException<string>(ex.Message, HttpStatusCode.InternalServerError);
            }
        }

        public List<logo> GetImagenesCliente()
        {
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS") return null;
            try
            {
                var request = HttpContext.Current.Request;
                int clv_comprador = Int32.Parse(request["id"].ToString());
                return SistemaController.GetImagenesCliente(clv_comprador);
            }
            catch (Exception ex)
            {
                throw new WebFaultException<string>(ex.Message, HttpStatusCode.InternalServerError);
            }
        }








        public int? GuardaImagenesProyecto()
        {
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS") return null;
            else
            {
                try
                {
                    var request = HttpContext.Current.Request;
                    //int clv_departamento =Int32.Parse(request["tipo"].ToString());                   

                    string URL = request["URL"].ToString();
                    if (request.Files.Count > 0)
                    {
                        int? clv_proyecto = Int32.Parse(request.Params["id"].ToString());
                        Stream streamed = request.Files[0].InputStream;
                        return SistemaController.GuardaImagenesProyecto(streamed, clv_proyecto);
                    }
                    else
                    {
                        throw new WebFaultException<string>("Archivo no encontrado", HttpStatusCode.ExpectationFailed);
                    }

                }
                catch (Exception ex)
                {
                    throw new WebFaultException<string>(ex.Message, HttpStatusCode.ExpectationFailed);
                }
            }

        }



        public int? GuardaImagenesDepartamento()
        {
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS") return null;
            else
            {
                try
                {
                    var request = HttpContext.Current.Request;
                    //int clv_departamento =Int32.Parse(request["tipo"].ToString());                   

                    string URL = request["URL"].ToString();
                    if (request.Files.Count > 0)
                    {
                        int? clv_departamento = Int32.Parse(request.Params["id"].ToString());
                        Stream streamed = request.Files[0].InputStream;
                        return SistemaController.GuardaImagenesDepartamento(streamed, clv_departamento);
                    } else
                    {
                        throw new WebFaultException<string>("Archivo no encontrado", HttpStatusCode.ExpectationFailed);
                    }

                }
                catch (Exception ex)
                {
                    throw new WebFaultException<string>(ex.Message, HttpStatusCode.ExpectationFailed);
                }
            }

        }
        public int? GuardaImagenesCliente()
        {
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS") return null;
            else
            {
                try
                {
                    var request = HttpContext.Current.Request;
                    //int clv_departamento =Int32.Parse(request["tipo"].ToString());                   

                    string URL = request["URL"].ToString();
                    if (request.Files.Count > 0)
                    {
                        int? clv_comprador = Int32.Parse(request.Params["id"].ToString());
                        Stream streamed = request.Files[0].InputStream;
                        return SistemaController.GuardaImagenesCliente(streamed, clv_comprador);
                    }
                    else
                    {
                        throw new WebFaultException<string>("Archivo no encontrado", HttpStatusCode.ExpectationFailed);
                    }

                }
                catch (Exception ex)
                {
                    throw new WebFaultException<string>(ex.Message, HttpStatusCode.ExpectationFailed);
                }
            }

        }


      





        #endregion
    }
}
