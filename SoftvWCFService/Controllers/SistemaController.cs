﻿
using AppWCFService.Services;
using Softv.Entities;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Web;

namespace AppWCFService.Controllers
{
    public class SistemaController
    {

        public  List<logo> GetImagenesDepartamento( int ? clv_departamento)
        {
            List<logo> lista = new List<logo>();
            try
            {
                dbHelper db = new dbHelper();                
                db.agregarParametro("@clv_departamento", SqlDbType.BigInt, clv_departamento);
                SqlDataReader rd = db.consultaReader("obtenImagenesDepartamento");
                while (rd.Read())
                {
                    logo l = new logo();
                    l.Valor = rd[2].ToString();
                    lista.Add(l);
                }
                rd.Close();
                db.conexion.Close();
                db.conexion.Dispose();
            }
            catch(Exception ex){}
           
           return lista;
        }



        public List<logo> GetImagenesProyecto(int? clv_departamento)
        {
            List<logo> lista = new List<logo>();
            try
            {
                dbHelper db = new dbHelper();
                db.agregarParametro("@clv_proyecto", SqlDbType.BigInt, clv_departamento);
                SqlDataReader rd = db.consultaReader("obtenImagenesProyecto");
                while (rd.Read())
                {
                    logo l = new logo();
                    l.Valor = rd[2].ToString();
                    lista.Add(l);
                }
                rd.Close();
                db.conexion.Close();
                db.conexion.Dispose();
            }
            catch (Exception ex) { }

            return lista;
        }



        public int? GuardaImagenesProyecto(Stream archivo, int? clv_proyecto)
        {

            string folder = System.AppDomain.CurrentDomain.BaseDirectory + "ImagenesProyectos/";
            string imagen = Guid.NewGuid().ToString() + ".png";
            System.Drawing.Image img = System.Drawing.Image.FromStream(archivo);
            if (!Directory.Exists(folder))
            {
                Directory.CreateDirectory(folder);
            }
            img.Save(folder + "\\" + imagen, ImageFormat.Png);

            dbHelper db = new dbHelper();
            db.agregarParametro("@clv_proyecto", SqlDbType.BigInt, clv_proyecto);
            db.agregarParametro("@Imagen", SqlDbType.VarChar, imagen);
            db.consultaSinRetorno("GuardaImagenProyecto");
            db.conexion.Close();
            db.conexion.Dispose();
            return 1;
        }

        public int ? GuardaImagenesDepartamento(Stream archivo, int ? clv_departamento) {

            string folder = System.AppDomain.CurrentDomain.BaseDirectory + "ImagenesDepartamentos/";
            string imagen = Guid.NewGuid().ToString() + ".png";
            System.Drawing.Image img = System.Drawing.Image.FromStream(archivo);
            if (!Directory.Exists(folder))
            {
                Directory.CreateDirectory(folder);
            }
            img.Save(folder + "\\" + imagen, ImageFormat.Png);

            dbHelper db = new dbHelper();
            db.agregarParametro("@clv_departamento", SqlDbType.BigInt, clv_departamento);
            db.agregarParametro("@Imagen", SqlDbType.VarChar, imagen);
            db.consultaSinRetorno("GuardaImagenDepartamento");
            db.conexion.Close();
           db.conexion.Dispose();            
            return 1;
        }


        public int? GuardaImagenesCliente(Stream archivo, int? clv_comprador)
        {

            string folder = System.AppDomain.CurrentDomain.BaseDirectory + "ImagenesClientes/";
            string imagen = Guid.NewGuid().ToString() + ".png";
            System.Drawing.Image img = System.Drawing.Image.FromStream(archivo);
            if (!Directory.Exists(folder))
            {
                Directory.CreateDirectory(folder);
            }
            img.Save(folder + "\\" + imagen, ImageFormat.Png);

            dbHelper db = new dbHelper();
            db.agregarParametro("@clv_comprador", SqlDbType.BigInt, clv_comprador);
            db.agregarParametro("@Imagen", SqlDbType.VarChar, imagen);
            db.consultaSinRetorno("GuardaImagenesCliente");
            db.conexion.Close();
            db.conexion.Dispose();
            return 1;
        }



        public List<logo> GetImagenesCliente(int ? clv_comprador)
        {
            List<logo> lista = new List<logo>();
            try
            {
                dbHelper db = new dbHelper();
                db.agregarParametro("@clv_comprador", SqlDbType.BigInt, clv_comprador);
                SqlDataReader rd = db.consultaReader("obtenImagenesCliente");
                while (rd.Read())
                {
                    logo l = new logo();
                    l.Valor = rd[2].ToString();
                    lista.Add(l);
                }
                rd.Close();
                db.conexion.Close();
                db.conexion.Dispose();
            }
            catch (Exception ex) { }

            return lista;
        }








    }
}