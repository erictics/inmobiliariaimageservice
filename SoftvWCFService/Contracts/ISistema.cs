﻿using Softv.Entities;
using System.Collections.Generic;
using System.ServiceModel;
using System.ServiceModel.Web;

namespace AppWCFService.Contracts
{
    [AuthenticatingHeader]
    [ServiceContract]
    public interface ISistema
    {
        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetImagenesDepartamento", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        List<logo> GetImagenesDepartamento();

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GuardaImagenesDepartamento", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Bare)]
        int? GuardaImagenesDepartamento();


        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetImagenesProyecto", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        List<logo> GetImagenesProyecto();

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GuardaImagenesProyecto", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? GuardaImagenesProyecto();


        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GuardaImagenesCliente", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? GuardaImagenesCliente();


        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetImagenesCliente", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        List<logo> GetImagenesCliente();




    }
}